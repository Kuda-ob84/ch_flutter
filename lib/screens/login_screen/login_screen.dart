import 'package:ch_flutter/screens/login_screen/bloc/login_screen_bloc.dart';
import 'package:ch_flutter/widget/save_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginScreenBloc>(
      create: (context) => LoginScreenBloc()..add(InitialLoginScreenEvent()),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xFF313033),
        appBar: _buildAppBar(),
        body: BlocBuilder<LoginScreenBloc, LoginScreenState>(
            builder: (context, state) {
          if (state is LoadingLoginScreenState) {
            return CircularProgressIndicator();
          } else if (state is InitialLoginScreenState) {
            return _buildScaffold(context, _buildPhoneAuth(context),
                isPhone: true);
          } else if (state is EmailAuthorizationScreenState) {
            return _buildScaffold(context, null, isEmail: true);
          } else if (state is LoginAuthorizationScreenState) {
            return _buildScaffold(context, _buildLoginAuth(context),
                isLogin: true);
          }
          return Container();
        }),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      backgroundColor: Color(0xFF313033),
      elevation: 0,
      actions: [
        IconButton(
            icon: SvgPicture.asset(
              "assets/icons_svg/ic_triple_dot.svg",
              color: Colors.white,
            ),
            onPressed: null)
      ],
    );
  }

  Widget _buildScaffold(BuildContext context, Widget authMode,
      {bool isPhone = false, bool isEmail = false, bool isLogin = false}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 74, top: 27),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SingleChildScrollView(
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Welcome to",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 32,
                          fontWeight: FontWeight.w300),
                    ),
                    Text(
                      "Connected Home",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: "HelveticaNeue",
                          fontSize: 32),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Easy and quick authorization",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 0.3),
                          fontWeight: FontWeight.w300,
                          fontSize: 15),
                    ),
                    const SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _buildInkWell(
                              "Phone",
                              () => BlocProvider.of<LoginScreenBloc>(context)
                                ..add(InitialLoginScreenEvent()),
                              isPhone),
                          _buildInkWell(
                              "Email",
                              () => BlocProvider.of<LoginScreenBloc>(context)
                                ..add(EmailAuthorizationScreenEvent()),
                              isEmail),
                          _buildInkWell(
                              "Login",
                              () => BlocProvider.of<LoginScreenBloc>(context)
                                ..add(LoginAuthorizationScreenEvent()),
                              isLogin),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Divider(color: Color.fromRGBO(255, 255, 255, 0.3)),
              SizedBox(height: 25),
              Padding(
                padding: const EdgeInsets.only(left: 33, right: 33, bottom: 10),
                child: authMode,
              )
            ]),
          ),
          InkWell(
            child: Text("Register",
                style: TextStyle(color: Color(0xFFFF6600), fontSize: 16)),
            onTap: () {
              print("Register");
            },
          )
        ],
      ),
    );
  }

  Widget _buildPhoneAuth(BuildContext context) {
    return Column(
      children: [
        _customTextField(
            context,
            BlocProvider.of<LoginScreenBloc>(context).phoneTextController,
            Icon(
              Icons.phone,
              color: Color.fromRGBO(255, 255, 255, 0.54),
              size: 30,
            ),
            "Phone"),
        SizedBox(height: 10),
        InkWell(
          onTap: () {
            print("Phone");
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 40,
            decoration: BoxDecoration(
                color: Color(0xFFFF6600),
                borderRadius: BorderRadius.circular(12)),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Next",
                      style: TextStyle(
                          fontFamily: "HelveticaNeue",
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700)),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _customTextField(BuildContext context,
      TextEditingController controller, Icon icon, String title) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        prefixIcon: icon,
        hintText: title,
        hintStyle: TextStyle(
            fontFamily: "HelveticaNeue",
            color: Color.fromRGBO(255, 255, 255, 0.54),
            fontSize: 16,
            fontWeight: FontWeight.w300),
        enabled: true,
        fillColor: Color.fromRGBO(255, 255, 255, 0.12),
        filled: true,
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              width: 1.0,
              style: BorderStyle.none,
            )),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1.0,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    );
  }

  Widget _buildInkWell(String text, Function onTap, bool isChecked) {
    return InkWell(
      child: Container(
        height: 32,
        width: 86,
        decoration: BoxDecoration(
            color: isChecked ? Color(0xFFFF6600) : Colors.transparent,
            borderRadius: BorderRadius.circular(20)),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            text,
            style: TextStyle(
                fontWeight: FontWeight.w400, fontSize: 16, color: Colors.white),
          ),
        ),
      ),
      onTap: onTap,
    );
  }

  Widget _buildLoginAuth(BuildContext context) {
    return Column(
      children: [
        _customTextField(
            context,
            BlocProvider.of<LoginScreenBloc>(context).loginTextController,
            Icon(
              Icons.phone,
              color: Color.fromRGBO(255, 255, 255, 0.54),
              size: 30,
            ),
            "Login"),
        SizedBox(height: 10),
        _customTextField(
            context,
            BlocProvider.of<LoginScreenBloc>(context).passwordTextController,
            Icon(
              Icons.lock,
              color: Color.fromRGBO(255, 255, 255, 0.54),
              size: 30,
            ),
            "Password"),
        SizedBox(height: 10),
        SaveLoginSwitch(
            value: BlocProvider.of<LoginScreenBloc>(context).rememberMe,
            onChange: null),
        SizedBox(height: 30),
        InkWell(
          onTap: () {
            print("Login");
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 40,
            decoration: BoxDecoration(
                color: Color(0xFFFF6600),
                borderRadius: BorderRadius.circular(12)),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Next",
                      style: TextStyle(
                          fontFamily: "HelveticaNeue",
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700)),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
