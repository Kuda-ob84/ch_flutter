import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'login_screen_event.dart';
part 'login_screen_state.dart';

class LoginScreenBloc extends Bloc<LoginScreenEvent, LoginScreenState> {
  LoginScreenBloc() : super(LoginScreenInitial());

  TextEditingController phoneTextController;
  TextEditingController loginTextController;
  TextEditingController passwordTextController;
  bool rememberMe;
  @override
  Stream<LoginScreenState> mapEventToState(
    LoginScreenEvent event,
  ) async* {
    if (event is InitialLoginScreenEvent) {
      yield InitialLoginScreenState();
    } else if (event is EmailAuthorizationScreenEvent) {
      yield EmailAuthorizationScreenState();
    } else if (event is LoginAuthorizationScreenEvent) {
      yield LoginAuthorizationScreenState();
    }
  }
}
