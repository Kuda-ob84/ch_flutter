part of 'login_screen_bloc.dart';

abstract class LoginScreenState extends Equatable {
  const LoginScreenState();

  @override
  List<Object> get props => [];
}

class LoginScreenInitial extends LoginScreenState {}

class LoadingLoginScreenState extends LoginScreenState {}

class InitialLoginScreenState extends LoginScreenState {}

class EmailAuthorizationScreenState extends LoginScreenState {}

class LoginAuthorizationScreenState extends LoginScreenState {}