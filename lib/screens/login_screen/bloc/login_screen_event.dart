part of 'login_screen_bloc.dart';

abstract class LoginScreenEvent extends Equatable {
  const LoginScreenEvent();

  @override
  List<Object> get props => [];
}

class InitialLoginScreenEvent extends LoginScreenEvent{}

class EmailAuthorizationScreenEvent extends LoginScreenEvent{}

class LoginAuthorizationScreenEvent extends LoginScreenEvent{}

