import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SaveLoginSwitch extends StatefulWidget {
  bool value;
  Function(bool) onChange;
  SaveLoginSwitch({@required this.value, @required this.onChange});
  @override
  _SaveLoginSwitchState createState() => _SaveLoginSwitchState();
}

class _SaveLoginSwitchState extends State<SaveLoginSwitch> {
  String checkCircle = "assets/icons_svg/check_circle.svg";
  String uncheckCircle = "assets/icons_svg/uncheck_circle.svg";
  bool checkedState = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 12),
      child: Row(
        children: [
          InkWell(
            child: SvgPicture.asset(checkedState ? checkCircle : uncheckCircle),
            onTap: () {
              setState(() {
                //checkedState = !checkedState;
              });
            },
          ),
          SizedBox(width: 10),
          Text(
            'Remember me',
            style: TextStyle(
                fontFamily: "HelveticaNeue",
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}
